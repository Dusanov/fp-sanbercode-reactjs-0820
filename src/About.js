import React from 'react';
import styles from './teststyle.module.css';
import gameEden from "./gameEden.jpg";
import allianceEmpire from "./allianceEmpire.jpg";
import lifeAfter from "./lifeAfter.jpg";

class About extends React.Component {
  render() {

    return (
    	<>
		  <div style={{borderBottom: "1px solid #444"}}>
		    <h1 style={{textAlign: "center"}}>Daftar Games Terbaik</h1>
		    <h3>Another Eden</h3>
		    <label><img src={gameEden} alt={"./gameEden.jpg"}/></label>
		    <h3 style={{float: "right", width: "45%"}}>Rating : 85</h3>
		    <p>Merupakan judul game terbaru karya Kato yang bisa kalian nikmati pada platform Android ataupun IOS,</p> 
		    <p>Hebat bukan main, game terbaru ini menghadirkan map yang sangat amat luas.</p>
		    <p>Konsep dari permainannya sendiri sangat mirip dengan kebanyakan JRPG </p>
		  </div>
	      

	      <div style={{borderBottom: "1px solid #444"}}>
	        <h3>Alliance vs Empire</h3>
	        <label><img src={allianceEmpire} alt={"./allianceEmpire.jpg"}/></label>
	        <h3 style={{float: "right", width: "45%"}}>Rating : 80</h3>
	        <p>AxE atau Alliance vs Empire merupakan game yang paling hangat di tahun 2019 ini,</p> 
	        <p>Lahir dari perusahaan asal Korea Selatan, Nexon Red.</p>
	        <p>Kalian juga akan bertarung dalam skala besar antara bangsa Alliance melawan Empire pada level tertentu.</p>
	      </div>

	      <div>
	        <h3>Life After</h3>
	        <label><img src={lifeAfter} alt={"./lifeAfter.jpg"}/></label>
	        <h3 style={{float: "right", width: "45%"}}>Rating : 70</h3>
	        <p>Kalian harus bertahan hidup di jaman kehancuran dunia setelah terjadinya penyebaran virus di muka bumi,</p> 
	        <p>Bertarung dan bertahan dari serangan – serangan zombie,</p> 
	        <p>Mencari bahan makanan yang bisa di makan.</p>
	      </div>
    </>
    	
    );

  }
}

export default About;