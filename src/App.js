import React from 'react';
import logo from './logo.svg';
import './App.css';
import Routes from "./Routes";
import Nav from "./Project/HeaderMovie";
import FooterMovie from "./Project/FooterMovie";
import SideBar from "./Project/SideBar";

import { BrowserRouter as Router } from "react-router-dom";

function App() {
  return (
    <Router>
      <SideBar />
      <Nav />
      <Routes />
      <FooterMovie />
    </Router>   
  );
}
  
export default App;

