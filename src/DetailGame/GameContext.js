import React, { useState, createContext } from "react";

export const GameContext = createContext();

export const GameProvider = props => {

	const [game_toSubmit, setGame_toSubmit] = useState(
		{
			name: "",
			genre: "",
			singlePlayer: "",
			multiplayer: "",
			platform: "",
			release: "",
			image_url: ""
		}
	)

	const [flagEdit, set_flagEdit] = useState(null)

	return (
		<GameContext.Provider value={[game_toSubmit, setGame_toSubmit, flagEdit, set_flagEdit]}>
			{props.children}
		</GameContext.Provider>
	);

};