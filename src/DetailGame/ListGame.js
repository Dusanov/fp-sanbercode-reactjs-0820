import React, {useState, useEffect, useContext} from "react"
import axios from 'axios';

import {UserContext} from "../LoginRegister/UserContext"
import {GameContext} from "./GameContext"

const ListGame = () => {

	// buat nyimpen data movie abis di GET
	const [dataGame, set_dataGame] = useState(null)

	// variable2 yang ada di context
	const [user,] = useContext(UserContext)
	const [game_toSubmit, setGame_toSubmit, flagEdit, set_flagEdit] = useContext(GameContext)

	// fungsi buat hapus
	const handle_hapusGame = (event) => {

		var idGame_yangMauDiDelete = parseInt(event.target.value);

		console.log(user.token)

		axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idGame_yangMauDiDelete}`, { headers: {"Authorization" : `Bearer ${user.token}`}})
			.then(res => {
				console.log(res);
			
				var new_dataGame = dataGame.filter(x=> x.id !== idGame_yangMauDiDelete);
				set_dataGame(new_dataGame);
		})

	}

	const handle_editGame = (event) => {

		//ambil id
		var idGame_yangMauDiEdit = event.target.value;

		//ambil objek
		var game_yangMauDiEdit = dataGame.find(x=> x.id == idGame_yangMauDiEdit);

		console.log(game_yangMauDiEdit)

		//ubah value dalam movie_toSubmit (yang ada di context)
		setGame_toSubmit(
			{
				/*
				title: movie_yangMauDiEdit.title,
				description: movie_yangMauDiEdit.description,
				year: movie_yangMauDiEdit.year,
				duration: movie_yangMauDiEdit.duration,
				genre: movie_yangMauDiEdit.genre,
				rating: movie_yangMauDiEdit.rating,
				review: movie_yangMauDiEdit.review,
				image_url: movie_yangMauDiEdit.image_url
				*/

				name: game_yangMauDiEdit.name ? game_yangMauDiEdit.name : "",
				genre: game_yangMauDiEdit.genre ? game_yangMauDiEdit.genre : "",
				singlePlayer: game_yangMauDiEdit.singlePlayer ? game_yangMauDiEdit.singlePlayer : "",
				multiplayer: game_yangMauDiEdit.multiplayer ? game_yangMauDiEdit.multiplayer : "",
				platform: game_yangMauDiEdit.platform ? game_yangMauDiEdit.platform : "",
				release: game_yangMauDiEdit.release ? game_yangMauDiEdit.release : "",
				image_url: game_yangMauDiEdit.image_url ? game_yangMauDiEdit.image_url : ""
			}
		)

		//ubah value dalam flagEdit (yang ada di context)
		set_flagEdit(parseInt(idGame_yangMauDiEdit))
	}

	useEffect( () => {
		if(dataGame == null){
			axios.get(`https://backendexample.sanbersy.com/api/data-game`)
				.then(res => {
					//console.log(res.data)
					set_dataGame(res.data)
			})
		}
	})

  	return (
		<>

			<h1>Tabel Daftar Game</h1>

			<table style={{border: "1px solid #000", padding: "2px", width:"100%"}}>
				<thead>
					<tr style={{backgroundColor: "grey"}}>
						<th style={{border: "1px solid #000", width:"35%"}}>Game Poster</th>
						<th style={{border: "1px solid #000", width:"15%"}}>Name</th>
						<th style={{border: "1px solid #000", width:"10%"}}>Genre</th>
						<th style={{border: "1px solid #000", width:"10%"}}>Release</th>
						<th style={{border: "1px solid #000", width:"10%"}}>Platform</th>
						<th style={{border: "1px solid #000", width:"20%"}}>Action</th>
					</tr>
				</thead>

				<tbody>
				{
					dataGame !== null && dataGame.map((item, index)=>{
						return(                    
							<tr key={item.id}>
								<td>
									<div>
										<img style={{maxWidth:"100%"}} src={item.image_url} />
									</div>
								</td>
								<td>{item.name}</td>
								<td>{item.genre}</td>
								<td>{item.release}</td>
								<td>{item.platform}</td>
								<td>
									<button value={item.id} onClick={handle_editGame}>Edit</button>
									&nbsp;
									<button value={item.id} onClick={handle_hapusGame}>Delete</button>
								</td>
							</tr>
						)
					})
				}
				</tbody>
			</table>

		</>
    )
}


export default ListGame;