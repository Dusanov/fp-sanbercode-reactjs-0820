import React, {useState, useContext} from "react"

import {UserContext} from "../LoginRegister/UserContext"
import {GameContext} from "./GameContext"

import axios from 'axios';

const SubmitGame = () => {

	const [user,] = useContext(UserContext)
	const [game_toSubmit, setGame_toSubmit, flagEdit, set_flagEdit] = useContext(GameContext)

	const handleChange = (event) =>{
		var value = event.target.value
		var name = event.target.name

		switch(name){
			case "name":{
				setGame_toSubmit({...game_toSubmit, name:value})
				break;
			}
			case "genre":{
				setGame_toSubmit({...game_toSubmit, genre:value})
				break;
			}
			case "singlePlayer":{
				setGame_toSubmit({...game_toSubmit, singlePlayer:value})
				break;
			}
			case "multiplayer":{
				setGame_toSubmit({...game_toSubmit, multiplayer:value})
				break;
			}
			case "platform":{
				setGame_toSubmit({...game_toSubmit, platform:value})
				break;
			}
			case "release":{
				setGame_toSubmit({...game_toSubmit, release:value})
				break;
			}
			case "image_url":{
				setGame_toSubmit({...game_toSubmit, image_url:value})
				break;
			}
		}
	}

	const ngilanginText_dalemForm = () => {
		setGame_toSubmit({
			name:"", 
			genre:"",
			singlePlayer: "",
			multiplayer: "",
			platform: "",
			release: "",
			image_url: ""
		})
	}

	const handle_submitGame = (event) => {

		// menahan submit
		event.preventDefault()

		var objek_denganToken = {
			token : user.token,
			name: game_toSubmit.name,
			genre: game_toSubmit.genre,
			singlePlayer: game_toSubmit.singlePlayer,
			multiplayer: game_toSubmit.multiplayer,
			platform: game_toSubmit.platform,
			release: game_toSubmit.release,
			image_url: game_toSubmit.image_url
		}

		console.log(objek_denganToken)

		// kalo flagEdit null maka dia submit
		if (flagEdit == null) {

			axios.post(`https://backendexample.sanbersy.com/api/data-game`, objek_denganToken)
				.then(
					response => {
						console.log(response.data);
						ngilanginText_dalemForm()
					}
				)
				.catch (
					(error)=>{
						alert(error)
					}
				)

		// kalo flagedit ga null, artinya kita mo edit, dan isinya flagedit merupakan id yang mau kita edit
		} else {

			axios.put(`https://backendexample.sanbersy.com/api/data-game/${flagEdit}`, objek_denganToken)
				.then(
					response => {
						console.log(response.data);
						ngilanginText_dalemForm()
					}
				)
				.catch (
					(error)=>{
						alert(error)
					}
				)
		}
	}

	return(
		<>
			<form onSubmit={handle_submitGame}>

				<label>Name:</label>
				<td></td>
				<input type="text" name="name" onChange={handleChange} value={game_toSubmit.name}/> <br/>

				<label>Genre:</label>
				<td></td>
				<input type="text" name="genre" onChange={handleChange} value={game_toSubmit.genre}/> <br/>

				<label>SinglePlayer:</label>
				<td></td>
				<input type="text" name="singlePlayer" onChange={handleChange} value={game_toSubmit.singlePlayer}/> <br/>

				<label>MultiPlayer:</label>
				<td></td>
				<input type="text" name="multiplayer" onChange={handleChange} value={game_toSubmit.multiplayer}/> <br/>

				<label>Platform:</label>
				<td></td>
				<input type="text" name="platform" onChange={handleChange} value={game_toSubmit.platform}/> <br/>

				<label>Release:</label>
				<td></td>
				<input type="text" name="release" onChange={handleChange} value={game_toSubmit.release}/> <br/>

				<label>Image Url:</label>
				<td></td>
				<input type="text" name="image_url" onChange={handleChange} value={game_toSubmit.image_url}/> <br/>

				<button>submit</button>

	        </form>
		</>
	)
}

export default SubmitGame;