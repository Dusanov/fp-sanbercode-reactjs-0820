import React, {useContext} from "react"
import {DaftarFilm} from "./DaftarFilm";

const FormFilm = () => {

  const [movie, setMovie, inputForm, setInputForm] = useContext(DaftarFilm)
  
  const handleSubmit = (event) => {
    
    event.preventDefault()

    var newId = movie.length +1

    if (inputForm.id === null) {
      setMovie([...movie, {title: inputForm.title, description:inputForm.description, years:inputForm.years, duration: inputForm.duration, genre: inputForm.genre, rating: inputForm.rating, id: newId}])
      }else{
        var singleMovie = movie.find(x=> x.id === inputForm.id)

        singleMovie.title = inputForm.title
        singleMovie.description = inputForm.description
        singleMovie.years = inputForm.years
        singleMovie.duration = inputForm.duration
        singleMovie.genre = inputForm.genre
        singleMovie.rating = inputForm.rating
        setMovie([...movie])
      }
    
    setInputForm({title: "", description: "", years:0, duration:"", genre: "", rating:"", id: null})  
  }
  
  const handleChangeTitle = (event) =>{
    setInputForm({...inputForm, title: event.target.value})
  
  }

  const handleChangeDescription = (event) =>{
    setInputForm({...inputForm, description: event.target.value})
    //setPrice(event.target.value)
  }

  const handleChangeYears = (event) =>{
    setInputForm({...inputForm, years: event.target.value})
    //setWeight(event.target.value)
  }

  const handleChangeDuration = (event) =>{
    setInputForm({...inputForm, duration: event.target.value})
    //setWeight(event.target.value)
  }

  const handleChangeGenre = (event) =>{
    setInputForm({...inputForm, genre: event.target.value})
    //setWeight(event.target.value)
  }

  const handleChangeRating = (event) =>{
    setInputForm({...inputForm, rating: event.target.value})
    //setWeight(event.target.value)
  }
          return(
            <>
              <h1>Movies List</h1>

              <form onSubmit={handleSubmit}>
                
                <strong>Title</strong><td><input type="text" value={inputForm.title} onChange={handleChangeTitle} /></td><br/>
                <td></td>
                <strong>Description</strong><td><input type="text" value={inputForm.description} onChange={handleChangeDescription} /> </td><br/>
                <td></td>
                <strong>Years</strong><td><input type="number" value={inputForm.years} onChange={handleChangeYears} /></td><br/>
                <td></td>
                <strong>Duration</strong><td><input type="text" value={inputForm.duration} onChange={handleChangeDuration} /></td><br/>
                <td></td>
                <strong>Genre</strong><td><input type="text" value={inputForm.genre} onChange={handleChangeGenre} /></td><br/>
                <td></td>
                <strong>Rating</strong><td><input type="text" value={inputForm.rating} onChange={handleChangeRating} /></td>
                <br/>
                <button>submit</button>
              
              </form>
            </>
          )
}

export default FormFilm;