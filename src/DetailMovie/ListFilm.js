import React, {useContext} from "react"
import {DaftarFilm} from "./DaftarFilm"


const ListFilm = () => {
  const [movie, setMovie, inputForm, setInputForm] = useContext(DaftarFilm)

  const handleDelete= (event) => {
  	var idMovie = parseInt(event.target.value)
  		var newMovie = movie.filter(x=> x.id !== idMovie)
  		setMovie([...newMovie])

  }

  const handleEdit= (event) => {
  		var idMovie = parseInt(event.target.value)
  		var singleMovie = movie.find(x=> x.id === idMovie)
  		setInputForm({...inputForm, title: singleMovie.title, description: singleMovie.description, years: singleMovie.years, duration: singleMovie.duration, genre: singleMovie.genre, rating: singleMovie.rating, id: idMovie})
  }

      return (
          <table>
           <thead>
            <tr>
              <th>ID</th>
              <th>Title</th>
              <th>Description</th>
              <th>Years</th>
              <th>Duration</th>
              <th>Genre</th>
              <th>Rating</th>
              <th>Action</th>
            </tr>
           </thead>
           <tbody>
          
            { movie.map(el=>{
              return (
                <tr>
                  <td>{el.id}</td>
                  <td>{el.title}</td>
                  <td>{el.description}</td>
                  <td>{el.years}</td>
                  <td>{el.duration}</td>
                  <td>{el.genre}</td>
                  <td>{el.rating}</td>
                  <td>
                    <button value={el.id} style={{marginRight:"10px"}}onClick={handleEdit}>Edit</button>
                    <button value={el.id} onClick={handleDelete}>Delete</button>
                  </td>
                </tr>
              )
                
            })}

           </tbody>
          </table>
      )
}

export default ListFilm;