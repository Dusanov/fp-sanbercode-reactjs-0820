import React, {useState, useEffect, useContext} from "react"
import axios from 'axios';

import {UserContext} from "../LoginRegister/UserContext"
import {MovieContext} from "./MovieContext"

const ListMovie = () => {

	// buat nyimpen data movie abis di GET
	const [dataMovie, set_dataMovie] = useState(null)

	// variable2 yang ada di context
	const [user,] = useContext(UserContext)
	const [movie_toSubmit, setMovie_toSubmit, flagEdit, set_flagEdit] = useContext(MovieContext)

	// fungsi buat hapus
	const handle_hapusMovie = (event) => {

		var idMovie_yangMauDiDelete = parseInt(event.target.value);

		console.log(user.token)

		axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${idMovie_yangMauDiDelete}`, { headers: {"Authorization" : `Bearer ${user.token}`}})
			.then(res => {
				console.log(res);
			
				var new_dataMovie = dataMovie.filter(x=> x.id !== idMovie_yangMauDiDelete);
				set_dataMovie(new_dataMovie);
		})

	}

	const handle_editMovie = (event) => {

		//ambil id
		var idMovie_yangMauDiEdit = event.target.value;

		//ambil objek
		var movie_yangMauDiEdit = dataMovie.find(x=> x.id == idMovie_yangMauDiEdit);

		console.log(movie_yangMauDiEdit)

		//ubah value dalam movie_toSubmit (yang ada di context)
		setMovie_toSubmit(
			{
				/*
				title: movie_yangMauDiEdit.title,
				description: movie_yangMauDiEdit.description,
				year: movie_yangMauDiEdit.year,
				duration: movie_yangMauDiEdit.duration,
				genre: movie_yangMauDiEdit.genre,
				rating: movie_yangMauDiEdit.rating,
				review: movie_yangMauDiEdit.review,
				image_url: movie_yangMauDiEdit.image_url
				*/

				title: movie_yangMauDiEdit.title ? movie_yangMauDiEdit.title : "",
				description: movie_yangMauDiEdit.description ? movie_yangMauDiEdit.description : "",
				year: movie_yangMauDiEdit.year ? movie_yangMauDiEdit.year : "",
				duration: movie_yangMauDiEdit.duration ? movie_yangMauDiEdit.duration : "",
				genre: movie_yangMauDiEdit.genre ? movie_yangMauDiEdit.genre : "",
				rating: movie_yangMauDiEdit.rating ? movie_yangMauDiEdit.rating : "",
				review: movie_yangMauDiEdit.review ? movie_yangMauDiEdit.review : "",
				image_url: movie_yangMauDiEdit.image_url ? movie_yangMauDiEdit.image_url : ""
			}
		)

		//ubah value dalam flagEdit (yang ada di context)
		set_flagEdit(parseInt(idMovie_yangMauDiEdit))
	}

	useEffect( () => {
		if(dataMovie == null){
			axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
				.then(res => {
					//console.log(res.data)
					set_dataMovie(res.data)
			})
		}
	})

  	return (
		<>

			<h1>Tabel Daftar Movie</h1>

			<table style={{border: "1px solid #000", padding: "2px", width:"100%"}}>
				<thead>
					<tr style={{backgroundColor: "grey"}}>
						<th style={{border: "1px solid #000", width:"35%"}}>Film Poster</th>
						<th style={{border: "1px solid #000", width:"15%"}}>Title</th>
						<th style={{border: "1px solid #000", width:"10%"}}>Year</th>
						<th style={{border: "1px solid #000", width:"10%"}}>Rating</th>
						<th style={{border: "1px solid #000", width:"10%"}}>Genre</th>
						<th style={{border: "1px solid #000", width:"20%"}}>Action</th>
					</tr>
				</thead>

				<tbody>
				{
					dataMovie !== null && dataMovie.map((item, index)=>{
						return(                    
							<tr key={item.id}>
								<td>
									<div>
										<img style={{maxWidth:"100%"}} src={item.image_url} />
									</div>
								</td>
								<td>{item.title}</td>
								<td>{item.year}</td>
								<td>{item.rating}</td>
								<td>{item.genre}</td>
								<td>
									<button value={item.id} onClick={handle_editMovie}>Edit</button>
									&nbsp;
									<button value={item.id} onClick={handle_hapusMovie}>Delete</button>
								</td>
							</tr>
						)
					})
				}
				</tbody>
			</table>

		</>
    )
}


export default ListMovie;

