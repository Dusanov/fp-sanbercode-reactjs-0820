import React, { useState, createContext } from "react";

export const MovieContext = createContext();

export const MovieProvider = props => {

	const [movie_toSubmit, setMovie_toSubmit] = useState(
		{
			title: "",
			description: "",
			year: "",
			duration: "",
			genre: "",
			rating: "",
			review: "",
			image_url: ""
		}
	)

	const [flagEdit, set_flagEdit] = useState(null)

	return (
		<MovieContext.Provider value={[movie_toSubmit, setMovie_toSubmit, flagEdit, set_flagEdit]}>
			{props.children}
		</MovieContext.Provider>
	);

};