import React, {useState, useContext} from "react"

import {UserContext} from "../LoginRegister/UserContext"
import {MovieContext} from "./MovieContext"

import axios from 'axios';

const SubmitMovie = () => {

	const [user,] = useContext(UserContext)
	const [movie_toSubmit, setMovie_toSubmit, flagEdit, set_flagEdit] = useContext(MovieContext)

	const handleChange = (event) =>{
		var value = event.target.value
		var name = event.target.name

		switch(name){
			case "title":{
				setMovie_toSubmit({...movie_toSubmit, title:value})
				break;
			}
			case "description":{
				setMovie_toSubmit({...movie_toSubmit, description:value})
				break;
			}
			case "year":{
				setMovie_toSubmit({...movie_toSubmit, year:value})
				break;
			}
			case "duration":{
				setMovie_toSubmit({...movie_toSubmit, duration:value})
				break;
			}
			case "genre":{
				setMovie_toSubmit({...movie_toSubmit, genre:value})
				break;
			}
			case "rating":{
				setMovie_toSubmit({...movie_toSubmit, rating:value})
				break;
			}
			case "review":{
				setMovie_toSubmit({...movie_toSubmit, review:value})
				break;
			}
			case "image_url":{
				setMovie_toSubmit({...movie_toSubmit, image_url:value})
				break;
			}
		}
	}

	const ngilanginText_dalemForm = () => {
		setMovie_toSubmit({
			title:"", 
			description:"",
			year: "",
			duration: "",
			genre: "",
			rating: "",
			review: "",
			image_url: ""
		})
	}

	const handle_submitFilm = (event) => {

		// menahan submit
		event.preventDefault()

		var objek_denganToken = {
			token : user.token,
			title: movie_toSubmit.title,
			description: movie_toSubmit.description,
			year: movie_toSubmit.year,
			duration: movie_toSubmit.duration,
			genre: movie_toSubmit.genre,
			rating: movie_toSubmit.rating,
			review: movie_toSubmit.review,
			image_url: movie_toSubmit.image_url
		}

		console.log(objek_denganToken)

		// kalo flagEdit null maka dia submit
		if (flagEdit == null) {

			axios.post(`https://backendexample.sanbersy.com/api/data-movie`, objek_denganToken)
				.then(
					response => {
						console.log(response.data);
						ngilanginText_dalemForm()
					}
				)
				.catch (
					(error)=>{
						alert(error)
					}
				)

		// kalo flagedit ga null, artinya kita mo edit, dan isinya flagedit merupakan id yang mau kita edit
		} else {

			axios.put(`https://backendexample.sanbersy.com/api/data-movie/${flagEdit}`, objek_denganToken)
				.then(
					response => {
						console.log(response.data);
						ngilanginText_dalemForm()
					}
				)
				.catch (
					(error)=>{
						alert(error)
					}
				)
		}
	}

	return(
		<>
			<form onSubmit={handle_submitFilm}>

				<label>Title:</label>
				<td></td>
				<input type="text" name="title" onChange={handleChange} value={movie_toSubmit.title}/> <br/>
				
				<label>Description:</label>
				<td></td>
				<input type="text" name="description" onChange={handleChange} value={movie_toSubmit.description}/> <br/>
				
				<label>Year:</label>
				<td></td>
				<input type="text" name="year" onChange={handleChange} value={movie_toSubmit.year}/> <br/>
				
				<label>Duration:</label>
				<td></td>
				<input type="text" name="duration" onChange={handleChange} value={movie_toSubmit.duration}/> <br/>
				
				<label>Genre:</label>
				<td></td>
				<input type="text" name="genre" onChange={handleChange} value={movie_toSubmit.genre}/> <br/>
				
				<label>Rating:</label>
				<td></td>
				<input type="text" name="rating" onChange={handleChange} value={movie_toSubmit.rating}/> <br/>
				
				<label>Review:</label>
				<td></td>
				<input type="text" name="review" onChange={handleChange} value={movie_toSubmit.review}/> <br/>
				
				<label>Image Url:</label>
				<td></td>
				<input type="text" name="image_url" onChange={handleChange} value={movie_toSubmit.image_url}/> <br/>

				<button>submit</button>

	        </form>
		</>
	)
}

export default SubmitMovie;