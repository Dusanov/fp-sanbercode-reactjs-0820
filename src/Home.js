import React from 'react';
import styles from './teststyle.module.css';
import marvelinvinity from "./marvelinvinity.jpg";
import mazerunnerposter from "./mazerunnerposter.jpg";
import korea from "./korea.jpg";


class Home extends React.Component {
  render() {

   return (
    <>

	  <div style={{borderBottom: "1px solid #444"}}>
	    <h1 style={{textAlign: "center"}}>Daftar Film Film Terbaik</h1>
	    <h3>Avenger Invinity War</h3>
	    <label><img src={marvelinvinity} alt={"./marvelinvinity.jpg"}/></label>
	    <h3 style={{float: "right", width: "34%"}}>Rating : 85</h3>
	    <p>The Avengers dan sekutu mereka harus bersedia mengorbankan segalanya</p> 
	    <p>dalam upaya untuk mengalahkan Thanos yang kuat sebelum ia berhasil menghancurkan alam semesta.</p>
	  </div>
      

      <div style={{borderBottom: "1px solid #444"}}>
        <h3>The Maze Runner</h3>
        <label><img src={mazerunnerposter} alt={"./mazerunnerposter.jpg"}/></label>
        <h3 style={{float: "right", width: "33%"}}>Rating : 80</h3>
        <p>Thomas kehilangan ingatan dan mendapati dirinya terjebak dalam labirin besar bernama Glade,</p> 
        <p>Bersama teman-temannya, ia memiliki kesempatan untuk meloloskan diri dan mendapatkan ingatannya kembali.</p>
      </div>

      <div>
        <h3>Its Okay To Not Be Okay</h3>
        <label><img src={korea} alt={"./korea.jpg"}/></label>
        <h3 style={{float: "right", width: "34%"}}>Rating : 70</h3>
        <p>Moon Gang-tae (Kim Soo-hyun), seorang pekerja kesehatan komunitas di bangsal psikiatri yang tidak punya waktu untuk cinta dan Ko Moon-young (Seo Ye-ji),</p> 
        <p>seorang penulis buku anak-anak sukses yang menderita gangguan kepribadian antisosial dan tidak pernah mengenal cinta.</p> 
        <p>Setelah bertemu, keduanya perlahan mulai menyembuhkan luka emosional satu sama lain. Di sepanjang serinya,</p>
        <p>hal lain juga mulai terungkap tentang kebenaran masa lalu mereka yang telah menghantui hidup mereka.</p>
      </div>
    </>
   );

  }
}

export default Home;