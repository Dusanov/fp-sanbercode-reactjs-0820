import React, { useState } from 'react';
 
function ListNamaFilm() {
  const dataList = [
    {
      "id": 1,
      "title": "avenger",
      "genre": "action",
      "rating": "80",
      "color": "grey"
    },
    {
      "id": 2,
      "title": "maze runner",
      "genre": "action",
      "rating": "70",
      "color": "white"
    },
    {
      "id": 3,
      "title": "annabele",
      "genre": "horror",
      "rating": "85",
      "color": "grey"
    },
    {
      "id": 4,
      "title": "si doel",
      "genre": "drama",
      "rating": "75",
      "color": "white"
    },
    {
      "id": 5,
      "title": "get out",
      "genre": "mistery",
      "rating": "90",
      "color": "grey"
    },
    {
      "id": 6,
      "title": "pengabdi setan",
      "genre": "horror",
      "rating": "77",
      "color": "white"
    },
    {
      "id": 7,
      "title": "interstelar",
      "genre": "scifi",
      "rating": "90",
      "color": "grey"
    },
    {
      "id": 8,
      "title": "ready or not",
      "genre": "horror",
      "rating": "82",
      "color": "white"
    },
    {
      "id": 9,
      "title": "joker",
      "genre": "mistery",
      "rating": "95",
      "color": "grey"
    },
    {
      "id": 10,
      "title": "love for sale",
      "genre": "drama",
      "rating": "89",
      "color": "white"
    },
    
  ];
 
  const [searchText, setSearchText] = useState("");
  const [data, setData] = useState(dataList);
 
  const excludeColumns = ["id", "color"];
 
  const handleChange = value => {
    setSearchText(value);
    filterData(value);
  };
 
  const filterData = (value) => {
    const lowercasedValue = value.toLowerCase().trim();
    if (lowercasedValue === "") setData(dataList);
    else {
      const filteredData = dataList.filter(item => {
        return Object.keys(item).some(key =>
          excludeColumns.includes(key) ? false : item[key].toString().toLowerCase().includes(lowercasedValue)
        );
      });
      setData(filteredData);
    }
  }
 
  return (
    <div className="App">
      Search: <input
        style={{ marginLeft: 5 }}
        type="text"
        placeholder="Type to search..."
        value={searchText}
        onChange={e => handleChange(e.target.value)}
      />
      <div className="box-container">
        {data.map((d, i) => {
          return <div key={i} className="box" style={{ backgroundColor: d.color }}>
            <b>Title: </b>{d.title}<br />
            <b>Genre: </b>{d.genre}<br />
            <b>Rating: </b>{d.rating}<br />
            <b>Color: </b>{d.color}<br />
          </div>
        })}
        <div className="clearboth"></div>
        {data.length === 0 && <span>Ga ada record di display!</span>}
      </div>
    </div>
  );
}
 
export default ListNamaFilm;