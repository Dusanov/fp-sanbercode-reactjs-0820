import React, {useState, useContext} from "react"
import {UserContext} from "./UserContext"
import axios from 'axios';

const Register = () => {

  	const [, setUser] = useContext(UserContext)
	const [input, setInput] = useState({email: "" , password: ""})

	const handleChange = (event) =>{
		let value = event.target.value
		let name = event.target.name

		switch (name){
			case "email":{
				setInput({...input, email: value})
				break;
			}
			case "password":{
				setInput({...input, password: value})
				break;
			}
			default:{
				break;
			}
		}
	}


	const handleSubmit = (event) =>{

		event.preventDefault()

		var objek_yangMauDiPost = {
			email: input.email, 
			password: input.password
		}

		console.log(objek_yangMauDiPost)

		axios.post("https://backendexample.sanbersy.com/api/user-login", objek_yangMauDiPost
			).then(
				(res)=>{

					console.log(res)

			        var user = res.data.user
			        var token = res.data.token
			        var currentUser = { name: user.name, email: user.email, token }

			        setUser(currentUser)

			        localStorage.setItem("user", JSON.stringify(currentUser))

			        alert('Berhasil Login!')
			        
				}
			).catch(
				(err)=>{
					alert(err)
				}
			)
	}

	return(
		<>
			<form onSubmit={handleSubmit}>

				<label>Email:</label>
				<input type="text" name="email" onChange={handleChange} /> <br/>

				<label>Password:</label>
				<input type="text" name="password" onChange={handleChange} /> <br/>

				<button>submit</button>

			</form>
		</>
	)
}

export default Register;