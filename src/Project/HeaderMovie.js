import React from "react";
import {Link} from "react-router-dom";
import {AppBar, Toolbar, Button} from '@material-ui/core';


const Nav = () => {
  return (
    <>
      <AppBar position="static">
        
        <Toolbar>

              <Link to="/">
                <Button variant = "contained" color = "secondary" style={{marginRight:"20px"}}>
                  Movie
                </Button>
              </Link>

              <Link to="/about">
                <Button variant = "contained" color = "secondary" style={{marginRight:"20px"}}>
                  Game
                </Button>
              </Link>

              <Link to="/listmovie">
                <Button variant = "contained" color = "secondary" style={{marginRight:"20px"}}>
                  List Movie
                </Button>
              </Link>

              <Link to="/register">
                <Button variant = "contained" color = "secondary" style={{marginRight:"20px"}}>
                  Register
                </Button>
              </Link>

              <Link to="/login">
                <Button variant = "contained" color = "secondary" style={{marginRight:"20px"}}>
                  Login
                </Button>
              </Link>

              <Link to="/listnamafilm">
                <Button variant = "contained" color = "secondary" style={{marginRight:"20px"}}>
                  List Nama Film
                </Button>
              </Link>

              <Link to="/listgame">
                <Button variant = "contained" color = "secondary" style={{marginRight:"20px"}}>
                  List Game
                </Button>
              </Link>

        </Toolbar>
        <Toolbar>
              <Button variant = "contained" color = "grey">
                <Link to="/detailfilm">Detail Movie</Link>
              </Button>
        </Toolbar>

      </AppBar>
    </>
  )
}

export default Nav