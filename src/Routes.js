import React from "react";
import { Switch, Route } from "react-router";

import Home from "./Home";
import About from "./About";
import ListNamaFilm from  "./ListNamaFilm";

import {TayanganProvider} from "./DetailMovie/DaftarFilm";
import ListFilm from "./DetailMovie/ListFilm";
import FormFilm from "./DetailMovie/FormFilm";

import {MovieProvider} from "./DetailMovie/MovieContext"
import SubmitMovie from "./DetailMovie/SubmitMovie";
import ListMovie from "./DetailMovie/ListMovie";

import {UserProvider} from "./LoginRegister/UserContext"
import Register from "./LoginRegister/Register";
import Login from "./LoginRegister/Login";

import {GameProvider} from "./DetailGame/GameContext"
import SubmitGame from "./DetailGame/SubmitGame";
import ListGame from "./DetailGame/ListGame";

import styles from './teststyle.module.css';

const Routes = () => {

  return (
    <Switch>
      
      <Route exact path="/">
        <div className={styles.containercontent}>
          <Home />
        </div>
      </Route>
      
      <Route exact path="/about">
        <div className={styles.containercontent}>
          <About />
        </div>
      </Route>

      <UserProvider>

        <MovieProvider>

          <Route exact path="/listmovie">
            <div className={styles.containercontent}>
              <SubmitMovie />
              <ListMovie />
            </div>
          </Route>

        </MovieProvider>

        <Route exact path="/register">
          <div className={styles.containercontent}>
            <Register />
          </div>
        </Route>

        <Route exact path="/login">
          <div className={styles.containercontent}>
            <Login />
          </div>
        </Route>

        <Route exact path="/listnamafilm">
          <div className={styles.containercontent}>
            <ListNamaFilm />
          </div>
        </Route>

        <GameProvider>

          <Route exact path="/listgame">
            <div className={styles.containercontent}>
              <SubmitGame />
              <ListGame />
            </div>
          </Route>

        </GameProvider>

      </UserProvider>
      
      <Route exact path="/detailfilm">
        <div className={styles.containercontent}>
          <TayanganProvider>
              <ListFilm /><br />
              <FormFilm />
          </TayanganProvider>
        </div>
      </Route>
    
    </Switch>

  );
};

export default Routes;